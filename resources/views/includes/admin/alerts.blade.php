@if($errors->any() || session('status'))
    <div id="alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="alert-modal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-{{ session('status') ? 'status' : 'errors' }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="alert-modal-label">{{ session('status') ? 'Успех' : 'Ошибка' }}</h4>
                </div>
                <div class="modal-body">
                    @if(session('status'))
                        @if(is_array(session('status')))
                            <ul>
                                @foreach(session('status') as $item)
                                    <li>{{ $item }}</li>
                                @endforeach
                            </ul>
                        @else
                            {{ session('status') }}
                        @endif
                    @else
                        <ul>
                            @foreach($errors->all() as $item)
                                <li>{{ $item }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-purple waves-effect" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    @section('js_bottom')
        @parent
        <script type="text/javascript">
            $(document).ready(function () {
              $('#alert-modal').modal();
            });
        </script>
    @endsection
@endif
