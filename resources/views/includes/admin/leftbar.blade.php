<ul>
    <?php
        /** @var  \App\Business\Support\Navigation\CurrentMenu  $current_menu */
    ?>
    <li class="text-muted menu-title">Меню</li>
    <li>
        <a href="{{ route('admin.catalog') }}" class="waves-effect waves-primary @if($current_menu->inLeft('catalog', 1)) subdrop @endif">
            <i class="md md-folder"></i>
            <span>Каталог объявлений</span>
        </a>
    </li>
</ul>
