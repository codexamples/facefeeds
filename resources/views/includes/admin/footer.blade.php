<footer class="footer text-right">
    {{ date('Y') }} © {{ config('app.name', 'Laravel') }}.
</footer>
