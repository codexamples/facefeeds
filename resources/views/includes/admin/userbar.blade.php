<div class="user-detail">
    <div class="dropup">
        <a
                href=""
                class="dropdown-toggle profile"
                data-toggle="dropdown"
                aria-expanded="true">
            <img src="{{ asset('assets/user.png') }}" alt="user-img" class="img-circle">
        </a>
        <ul class="dropdown-menu">
            <li class="m-b-15">
                <a href="{{ route('admin.password') }}">
                    <i class="md md-vpn-key m-r-10"></i> Сменить пароль
                </a>
            </li>
            <li>
                <a href="#" onclick="document.getElementById('logout-form').submit();">
                    <i class="md md-settings-power m-r-10"></i> Выход
                </a>
                <form method="post" action="{{ route('logout') }}" id="logout-form">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
    <h5 class="m-t-0 m-b-0">{{ $auth->name }}</h5>
    <p class="text-muted m-b-0">ID : {{ $auth->id }}</p>
</div>
