<ol class="breadcrumb pull-right">
    <?php
        /** @var  \App\Business\Support\Navigation\Breadcrumb  $breadcrumb */
    ?>
    @section('title', $breadcrumb->last()->caption())
    @while(true)
        @if($breadcrumb->hasNext())
            <li><a href="{{ $breadcrumb->url() }}">{{ $breadcrumb->caption() }}</a></li>
            @php($breadcrumb = $breadcrumb->next())
        @else
            <li class="active">{{ $breadcrumb->caption() }}</li>
            @break
        @endif
    @endwhile
</ol>
