@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form
                        method="post"
                        action="{{ route('admin.password.save') }}"
                >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Старый пароль</label>
                        <input
                                type="password"
                                class="form-control"
                                name="old_password"
                                value=""
                        >
                    </div>
                    <div class="form-group">
                        <label>Новый пароль</label>
                        <input
                                type="password"
                                class="form-control"
                                name="password"
                                value=""
                        >
                    </div>
                    <div class="form-group">
                        <label>Повтор нового пароля</label>
                        <input
                                type="password"
                                class="form-control"
                                name="password_confirmation"
                                value=""
                        >
                    </div>
                    <div class="form-group m-t-40">
                        <button class="btn btn-success btn-custom pull-right">
                            <i class="md md-save"></i>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection
