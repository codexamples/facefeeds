@extends('layouts.admin')

@section('content')
    <?php /** @var \Illuminate\Pagination\LengthAwarePaginator $catalogs */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="m-b-15">
                    <a
                            href="{{ route('admin.catalog.create') }}"
                            class="pull-right btn btn-inverse btn-custom"
                    >
                        <i class="fa fa-plus"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th><i class="md-star-half"></i></th>
                        <th>Название</th>
                        <th>Объявления</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($catalogs as $catalog)
                        <?php /** @var  \App\Models\Catalog  $catalog */ ?>
                            @if($invalids = $catalog->getInvalidCount())
                                <tr class="danger">
                                    <td title="Выгрузка содержит невалидных объявлений: {{ $invalids }}"><i class="md md-star-outline"></i></td>
                            @else
                                <tr>
                                    <td title="Выгрузка валидна"><i class="md md-star"></i></td>
                            @endif
                            <td title="{{ $catalog->key }}">{{ $catalog->name }}</td>
                            <td>
                                <a href="{{ route('admin.ad', $catalog->key) }}" title="Посмотреть все">
                                    {{ $catalog->ads()->count() }}
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <td>
                                <a
                                        href="{{ route('admin.catalog.edit', $catalog->key) }}"
                                        class="btn btn-success btn-custom"
                                        title="Редактировать"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a
                                        href="{{ route('admin.catalog.destroy', $catalog->key) }}"
                                        class="btn btn-danger btn-custom m-l-5 destroy-btn"
                                        title="Удалить"
                                >
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="4" class="text-center">&lt; Пусто &gt;</td></tr>
                    @endforelse
                    </tbody>
                </table>
                {!! $catalogs->links() !!}
            </div>
        </div>
    </div>
@endsection
