@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\Catalog  $catalog */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form method="post" action="{{ $catalog ? route('admin.catalog.update', $catalog->key) : route('admin.catalog.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if($catalog)
                        <div class="form-group">
                            <label>Ключ</label>
                            <input
                                    type="text"
                                    class="form-control"
                                    value="{{ $catalog->key }}"
                                    readonly
                            >
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Название</label>
                        <input
                                type="text"
                                class="form-control"
                                name="name"
                                value="{{ $catalog->name or old('name') }}"
                        >
                    </div>
                    <div class="form-group">
                        <label>Выгрузка CSV</label>
                        <input
                                type="file"
                                class="form-control"
                                name="file"
                        >
                    </div>
                    @if($catalog)
                        <div class="form-group">
                            <label>Объявления</label>
                            <div class="form-control">
                                <a href="{{ route('admin.ad', $catalog->key) }}">Посмотреть все ({{ $catalog->ads()->count() }})</a>
                            </div>
                        </div>
                    @endif
                    <div class="form-group m-t-40">
                        <button class="btn btn-success btn-custom pull-right">
                            <i class="md md-save"></i>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection
