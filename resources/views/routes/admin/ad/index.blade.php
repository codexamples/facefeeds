@extends('layouts.admin')

@section('content')
    <?php /** @var \App\Models\Catalog $parent_catalog */ ?>
    <?php /** @var \Illuminate\Pagination\LengthAwarePaginator $ads */ ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form method="GET" action="{{ route('admin.ad', $parent_catalog->key) }}" class="m-b-15">
                    <div class="col-md-10">
                        <div class="form-group">
                            <input
                                    class="form-control"
                                    type="text"
                                    name="q"
                                    value="{{ $q }}"
                                    placeholder="Поиск"
                            >
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-block btn-info btn-custom">
                            <i class="md md-search"></i>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </form>
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th><i class="md-star-half"></i></th>
                        <th>№</th>
                        <th>Данные</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($ads as $ad)
                        <?php /** @var  \App\Models\Ad  $ad */ ?>
                        @if($validation = $ad->validation)
                            <tr class="danger">
                                <td title="Объявление содержит ошибки"><i class="md md-star-outline"></i></td>
                        @else
                            <tr>
                                <td title="Объявление корректно"><i class="md md-star"></i></td>
                        @endif
                            <td>{{ $ad->id }}</td>
                            <td>
                                @if($data = $ad->data)
                                    <div>Полей: <b>{{ count($data) }}</b></div>
                                    @if($q)
                                        <div class="text-muted">
                                            <div><i>Поисковые соответствия:</i></div>
                                            <ul>
                                                @foreach($data as $key => $value)
                                                    @php($kpos = mb_strpos($key, $q))
                                                    @php($vpos = mb_strpos($value, $q))
                                                    @if($kpos !== false || $vpos !== false)
                                                        <li>
                                                            <div><b>{{ str_limit($key, 20) }}</b></div>
                                                            <div>{{ str_limit($value, 20) }}</div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                @endif
                                @if($validation)
                                    <div>Ошибок валидации: <b>{{ count($validation) }}</b></div>
                                @endif
                            </td>
                            <td>
                                <a
                                        href="{{ route('admin.ad.edit', [$parent_catalog->key, $ad->id]) }}"
                                        class="btn btn-success btn-custom"
                                        title="Редактировать"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <button
                                        class="btn btn-warning btn-custom preview-modal-btn"
                                        title="Превью"
                                        data-preview="{{ json_encode($ad->data) }}"
                                >
                                    <i class="fa fa-dedent"></i>
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="4" class="text-center">&lt; Пусто &gt;</td></tr>
                    @endforelse
                    </tbody>
                </table>
                {!! $ads->links() !!}
            </div>
        </div>
    </div>
    <div id="preview-modal-box" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="previewModalLabel">Превью</h4>
                </div>
                <div class="modal-body" id="preview-modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js_bottom')
    @parent
    <script type="text/javascript" src="{{ asset('assets/js/preview_modal.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            previewModal.start('preview-modal', function(data) {
                var arr = [];

                if (data.name) {
                    arr.push('<h4>' + data.name + '</h4>');
                }

                if (data['image:url']) {
                    arr.push('<img src="' + data['image:url'] + '" width="128" style="float: left;">');
                }

                if (data.description) {
                    arr.push('<p>' + data.description + '</p>');
                }

                if (data.price) {
                    arr.push(data.price);
                }

                if (arr.length > 0) {
                    arr.push('<div class="clearfix"></div>');
                }

                return arr;
            });
        });
    </script>
@endsection
