@extends('layouts.admin')

@section('content')
    <?php /** @var  \App\Models\Ad  $ad */ ?>
    <?php /** @var  \App\Models\Catalog  $parent_catalog */ ?>
    @php($parent_catalog = $ad->catalog)
    @if($validation = $ad->validation)
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <h4>Объявление содержит ошибки:</h4>
                    <ol>
                        @foreach($validation as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form method="post" action="{{ route('admin.ad.update', [$parent_catalog->key, $ad->id]) }}">
                    {{ csrf_field() }}
                    <div id="ad_fields"></div>
                    <div class="col-md-12 m-t-10">
                        <div class="col-md-10"></div>
                        <div class="col-md-2">
                            <button class="btn btn-block btn-custom btn-info" id="ad_add" title="Добавить новое поле">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group m-t-40">
                        <button class="btn btn-success btn-custom pull-right">
                            <i class="md md-save"></i>
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_bottom')
    @parent
    <script type="text/javascript" src="{{ asset('assets/js/dyn_fields.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            dynFields.start({!! json_encode($ad->data, JSON_UNESCAPED_UNICODE) !!}, "ad_");
        });
    </script>
@endsection
