<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('meta')

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    @yield('css')

    @yield('js_top')
</head>
<body @yield('body_tag') >
@yield('body_content')

@yield('js_bottom')
</body>
</html>
