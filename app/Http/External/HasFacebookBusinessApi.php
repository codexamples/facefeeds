<?php

namespace App\Http\External;

use App\Business\Http\FacebookBusinessApi;

/**
 * Класс, имеющий клиента API FB-Graph Business, как синглтон
 */
trait HasFacebookBusinessApi
{
    /**
     * Клиент
     *
     * @var \App\Business\Http\FacebookBusinessApi
     */
    private $facebookBusinessApi;

    /**
     * Возвращает клиента
     *
     * @return \App\Business\Http\FacebookBusinessApi
     */
    protected function facebookBusinessApi()
    {
        if ($this->facebookBusinessApi === null) {
            $this->facebookBusinessApi = app()->make(FacebookBusinessApi::class);
        }

        return $this->facebookBusinessApi;
    }
}
