<?php

namespace App\Http\External;

use App\Business\Http\FacebookBusinessApi as FacebookBusinessApiContract;
use Exception;

/**
 * Реализация клиента API https://graph.facebook.com/ - business
 */
class FacebookBusinessApi implements FacebookBusinessApiContract
{
    /**
     * Версия API
     *
     * @var string
     */
    protected $version;

    /**
     * Идентификатор бизнес-страницы
     *
     * @var string
     */
    protected $businessId;

    /**
     * Токен доступа
     *
     * @var string
     */
    protected $token;

    /**
     * Создает экземпляр, задавая поля по умолчанию
     */
    public function __construct()
    {
        if ($this->version === null) {
            $this->version = env('FB_VERSION', 'none');
        }

        if ($this->businessId === null) {
            $this->businessId = env('FB_BUSINESS_ID', 'none');
        }

        if ($this->token === null) {
            $this->token = env('FB_TOKEN', 'none');
        }
    }

    /**
     * Строит и возвращает ссылку для вызова API
     *
     * @param  array  $parts
     * @return string
     */
    protected function buildUrl(array $parts)
    {
        array_unshift($parts, $this->version);
        array_unshift($parts, 'https://graph.facebook.com');

        return implode('/', $parts);
    }

    /**
     * Обертка над возвращением ответа от FB
     *
     * @param  $ch
     * @param  callable  $callback
     * @return mixed
     * @throws \Exception
     */
    protected function respond($ch, callable $callback)
    {
        $response = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($response, true);

        try {
            return call_user_func($callback, $data);
        } catch (Exception $ex) {
            $msg = is_array($data)
                && array_key_exists('error', $data)
                && is_array($data['error'])
                && array_key_exists('message', $data['error'])
                ? $data['error']['message']
                : $response;

            throw new Exception($msg);
        }
    }

    /**
     * Создает новый каталог объявлений
     *
     * @param  string  $name
     * @return string
     */
    public function createProductCatalog(string $name)
    {
        $ch = curl_init($this->buildUrl([$this->businessId, 'product_catalogs']) . '?' . http_build_query([
            'access_token' => $this->token,
            'vertical' => 'home_listings',
            'name' => $name,
        ]));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $this->respond($ch, function ($data) {
            return $data['id'];
        });
    }

    /**
     * Удаляет каталог объявлений
     *
     * @param  string  $catalog_id
     * @return bool
     */
    public function deleteProductCatalog(string $catalog_id)
    {
        $ch = curl_init($this->buildUrl([$catalog_id]) . '?' . http_build_query(['access_token' => $this->token]));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $this->respond($ch, function ($data) {
            return $data['success'];
        });
    }

    /**
     * Переименовывает каталог объявлений
     *
     * @param  string  $catalog_id
     * @param  string  $new_name
     * @return bool
     */
    public function renameProductCatalog(string $catalog_id, string $new_name)
    {
        $ch = curl_init($this->buildUrl([$catalog_id]) . '?' . http_build_query([
            'access_token' => $this->token,
            'name' => $new_name,
        ]));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $this->respond($ch, function ($data) {
            return $data['success'];
        });
    }

    /**
     * Создает новое место для выгрузки
     *
     * @param  string  $catalog_id
     * @return string
     */
    public function createProductFeed(string $catalog_id)
    {
        $ch = curl_init($this->buildUrl([$catalog_id, 'product_feeds']) . '?' . http_build_query([
            'access_token' => $this->token,
            'country' => 'RU',
            'default_currency' => 'RUB',
            'name' => 'Auto-Generated From FaceFeeds',
        ]));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $this->respond($ch, function ($data) {
            return $data['id'];
        });
    }

    /**
     * Загружает выгрузку в каталог объявлений
     *
     * @param  string  $feed_id
     * @param  string  $url
     * @return string
     */
    public function uploadFeed(string $feed_id, string $url)
    {
        $ch = curl_init($this->buildUrl([$feed_id, 'uploads']) . '?' . http_build_query([
            'access_token' => $this->token,
            'url' => $url,
        ]));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $this->respond($ch, function ($data) {
            return $data['id'];
        });
    }
}
