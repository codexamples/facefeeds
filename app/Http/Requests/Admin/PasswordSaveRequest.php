<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class PasswordSaveRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required|min:4|confirmed',
        ];
    }
}
