<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class CatalogCrudRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:32|unique:catalogs,name,' . $this->key . ',key',
            'file' => ($this->key ? 'nullable' : 'required') . '|max:10240|mimes:csv,txt',
        ];
    }
}
