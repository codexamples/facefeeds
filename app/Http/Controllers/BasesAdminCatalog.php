<?php

namespace App\Http\Controllers;

use App\Models\Repositories\CatalogRepository;
use App\Support\Navigation\HasBreadcrumb;
use App\Support\Navigation\HasCurrentMenu;

trait BasesAdminCatalog
{
    use HasBreadcrumb, HasCurrentMenu;

    /**
     * Репозиторий каталогов
     *
     * @var \App\Models\Repositories\CatalogRepository
     */
    private $catalogs;

    /**
     * Родительский каталог
     *
     * @var \App\Models\Catalog|null
     */
    private $parentCatalog;

    /**
     * Возвращает репозиторий каталогов
     *
     * @return \App\Models\Repositories\CatalogRepository
     */
    protected function catalogs()
    {
        return $this->catalogs;
    }

    /**
     * Возвращает родительский каталог
     *
     * @return \App\Models\Catalog|null
     */
    protected function parentCatalog()
    {
        return $this->parentCatalog;
    }

    /**
     * Вспомощь конструктору
     *
     * @param  \App\Models\Repositories\CatalogRepository  $catalogs
     * @return void
     */
    protected function startup(CatalogRepository $catalogs)
    {
        $this->breadcrumb()->append(route('admin.catalog'), 'Каталоги объявлений');
        $this->currentMenu()->addLeft('catalog');

        $this->catalogs = $catalogs;
    }

    /**
     * Инициализирует родительский каталог, включая хлебные крошки
     *
     * @param  string  $key
     * @return void
     */
    protected function findParentCatalog(string $key)
    {
        $this->parentCatalog = $this->catalogs()->one($key);

        $this->breadcrumb()->append(
            route('admin.catalog.edit', $this->parentCatalog()->key),
            $this->parentCatalog()->name
        );
    }
}
