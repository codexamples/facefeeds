<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BasesAdminCatalog;
use App\Http\Requests\Admin\CatalogCrudRequest;
use App\Models\Repositories\CatalogRepository;

class CatalogController extends Controller
{
    use BasesAdminCatalog;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\CatalogRepository  $catalogs
     */
    public function __construct(CatalogRepository $catalogs)
    {
        $this->startup($catalogs);
    }

    /**
     * Список всех каталогов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('routes.admin.catalog.index', $this->fillViewData([
            'catalogs' => $this->catalogs()->all(),
        ]));
    }

    /**
     * Форма создания каталога
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->breadcrumb()->append(route('admin.catalog.create'), 'Новый каталог');

        return view('routes.admin.catalog.edit', $this->fillViewData([
            'catalog' => null,
        ]));
    }

    /**
     * Форма редактирования каталога
     *
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function edit(string $key)
    {
        $this->findParentCatalog($key);

        return view('routes.admin.catalog.edit', $this->fillViewData([
            'catalog' => $this->parentCatalog(),
        ]));
    }

    /**
     * Создание каталога
     *
     * @param  \App\Http\Requests\Admin\CatalogCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatalogCrudRequest $request)
    {
        return redirect()
            ->route('admin.catalog.edit', $this->catalogs()->create($request)->key)
            ->with('status', 'Каталог создан');
    }

    /**
     * Обновление каталога
     *
     * @param  \App\Http\Requests\Admin\CatalogCrudRequest  $request
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function update(CatalogCrudRequest $request, string $key)
    {
        return redirect()
            ->route('admin.catalog.edit', $this->catalogs()->update($key, $request)->key)
            ->with('status', 'Каталог обновлен');
    }

    /**
     * Обновление каталога
     *
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $key)
    {
        $this->catalogs()->destroy($key);

        return redirect()
            ->route('admin.catalog')
            ->with('status', 'Каталог удален');
    }
}
