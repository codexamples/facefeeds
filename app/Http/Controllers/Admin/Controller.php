<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use App\Support\Navigation\HasBreadcrumb;
use App\Support\Navigation\HasCurrentMenu;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use HasBreadcrumb, HasCurrentMenu;

    /**
     * Возвращает дополненные данные для вьюх
     *
     * @param  array  $data
     * @return array
     */
    protected function fillViewData(array $data = [])
    {
        return array_merge($data, [
            'auth' => Auth::user(),
            'breadcrumb' => $this->breadcrumb(),
            'current_menu' => $this->currentMenu(),
        ]);
    }
}
