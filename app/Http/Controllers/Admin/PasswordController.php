<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PasswordSaveRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    /**
     * Создает новый экземпляр контроллера
     */
    public function __construct()
    {
        $this->breadcrumb()->append(route('admin.password'), 'Пароль');
        $this->currentMenu()->addLeft('password');
    }

    /**
     * Форма смены админского пароля
     *
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        return view('routes.admin.password', $this->fillViewData());
    }

    /**
     * Изменение админского пароля
     *
     * @param  \App\Http\Requests\Admin\PasswordSaveRequest  $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function save(PasswordSaveRequest $request)
    {
        /** @var  \App\Business\Authorization\PasswordAccessible  $auth */
        $auth = Auth::user();

        if ($auth->passwordAsserts($request->input('old_password'))) {
            $auth->changePassword($request->input('password'));

            return redirect()->route('admin.password')->with('status', 'Пароль сменен');
        }

        /** @var  \Illuminate\Validation\Validator  $validator */
        $validator = Validator::make([], []);

        $validator->messages()->add('password.wrong', 'Старый пароль неверный');
        return redirect()->route('admin.password')->withErrors($validator);
    }
}
