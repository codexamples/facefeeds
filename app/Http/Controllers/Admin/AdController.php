<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BasesAdminCatalog;
use App\Http\External\HasFacebookBusinessApi;
use App\Http\Requests\Admin\AdUpdateRequest;
use App\Models\Repositories\AdRepository;
use App\Models\Repositories\CatalogRepository;
use Illuminate\Http\Request;

class AdController extends Controller
{
    use BasesAdminCatalog {
        findParentCatalog as baseFindParentCatalog;
    }

    use HasFacebookBusinessApi;

    /**
     * Репозиторий объявлений
     *
     * @var \App\Models\Repositories\AdRepository
     */
    private $ads;

    /**
     * Создает новый экземпляр контроллера
     *
     * @param  \App\Models\Repositories\CatalogRepository  $catalogs
     * @param  \App\Models\Repositories\AdRepository  $ads
     */
    public function __construct(CatalogRepository $catalogs, AdRepository $ads)
    {
        $this->startup($catalogs);
        $this->ads = $ads;
    }

    /**
     * Возвращает репозиторий объявлений
     *
     * @return \App\Models\Repositories\AdRepository
     */
    protected function ads()
    {
        return $this->ads;
    }

    /**
     * Инициализирует родительский каталог, включая хлебные крошки
     *
     * @param  string  $key
     * @return void
     */
    protected function findParentCatalog(string $key)
    {
        $this->baseFindParentCatalog($key);

        $this->breadcrumb()->append(route('admin.ad', $this->parentCatalog()->key), 'Объявления');
        $this->currentMenu()->addLeft('ad');
    }

    /**
     * Список всех объявлений
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $catalog_key
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, string $catalog_key)
    {
        $this->findParentCatalog($catalog_key);

        return view('routes.admin.ad.index', $this->fillViewData([
            'parent_catalog' => $this->parentCatalog(),
            'ads' => $this->ads()->allFromParent($this->parentCatalog()->key, $request),
            'q' => $request->input('q'),
        ]));
    }

    /**
     * Форма редактирования объявления
     *
     * @param  string  $catalog_key
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $catalog_key, int $id)
    {
        $this->findParentCatalog($catalog_key);
        $ad = $this->ads()->one($id);

        $this->breadcrumb()->append(
            route('admin.ad.edit', [$this->parentCatalog()->key, $ad->id]),
            'Объявление №' . $ad->id
        );

        return view('routes.admin.ad.edit', $this->fillViewData([
            'ad' => $ad,
        ]));
    }

    /**
     * Обновление каталога
     *
     * @param  \App\Http\Requests\Admin\AdUpdateRequest  $request
     * @param  string  $catalog_key
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdUpdateRequest $request, string $catalog_key, int $id)
    {
        $this->findParentCatalog($catalog_key);

        $ad = $this->ads()->updateFromParent($catalog_key, $id, $request);

        $this->parentCatalog()->exportFeed();
        $this->facebookBusinessApi()->uploadFeed($this->parentCatalog()->feed_id, $this->parentCatalog()->getFeedUrl());

        return redirect()
            ->route('admin.ad.edit', [$this->parentCatalog()->key, $ad->id])
            ->with('status', 'Каталог обновлен');
    }
}
