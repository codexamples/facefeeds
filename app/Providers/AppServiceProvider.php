<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Business\Support\Navigation\Breadcrumb::class,
            function () {
                return \App\Support\Navigation\Breadcrumb::start(route('admin'), config('app.name', 'Laravel'));
            }
        );

        $this->app->bind(
            \App\Business\Support\Navigation\CurrentMenu::class,
            function () {
                return \App\Support\Navigation\CurrentMenu::start()->addLeft('index');
            }
        );

        $this->app->bind(
            \App\Business\Http\FacebookBusinessApi::class,
            \App\Http\External\FacebookBusinessApi::class
        );
    }
}
