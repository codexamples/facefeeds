<?php

namespace App\Business\Http;

/**
 * Клиент API https://graph.facebook.com - business
 */
interface FacebookBusinessApi
{
    /**
     * Создает новый каталог объявлений
     *
     * @param  string  $name
     * @return int
     */
    public function createProductCatalog(string $name);

    /**
     * Удаляет каталог объявлений
     *
     * @param  string  $catalog_id
     * @return bool
     */
    public function deleteProductCatalog(string $catalog_id);

    /**
     * Переименовывает каталог объявлений
     *
     * @param  string  $catalog_id
     * @param  string  $new_name
     * @return bool
     */
    public function renameProductCatalog(string $catalog_id, string $new_name);

    /**
     * Создает новое место для выгрузки
     *
     * @param  string  $catalog_id
     * @return string
     */
    public function createProductFeed(string $catalog_id);

    /**
     * Загружает выгрузку в каталог объявлений
     *
     * @param  string  $feed_id
     * @param  string  $url
     * @return string
     */
    public function uploadFeed(string $feed_id, string $url);
}
