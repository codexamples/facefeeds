<?php

namespace App\Business\Support\Navigation;

/**
 * Информация о текущих пунктах меню
 *
 * @method string getLeft()
 * @method bool inLeft(string $part, int $level)
 * @method \App\Support\Navigation\CurrentMenu setLeft(string $full)
 * @method \App\Support\Navigation\CurrentMenu addLeft(string $part)
 */
interface CurrentMenu
{
}
