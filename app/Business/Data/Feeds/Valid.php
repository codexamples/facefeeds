<?php

namespace App\Business\Data\Feeds;

/**
 * Валидация выгрузки
 */
interface Valid
{
    /**
     * Возвращает количество невалидных объявлений из выгрузки
     *
     * @return int
     */
    public function getInvalidCount();
}
