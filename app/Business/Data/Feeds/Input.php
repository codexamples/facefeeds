<?php

namespace App\Business\Data\Feeds;

use Illuminate\Http\UploadedFile;

/**
 * Парсинг выгрузки
 */
interface Input
{
    /**
     * Парсит выгрузку, создавая объекты объявлений
     * Возвращает массив созданных объявлений
     *
     * @param  \Illuminate\Http\UploadedFile  $file
     * @return array
     */
    public function parseFeed(UploadedFile $file);
}
