<?php

namespace App\Business\Data\Feeds;

/**
 * Экспорт выгрузки для Facebook
 */
interface Output
{
    /**
     * Возвращает ссылку на выгрузку для Facebook
     *
     * @return string
     */
    public function getFeedUrl();

    /**
     * Экспортирует выгрузку для Facebook
     *
     * @return void
     */
    public function exportFeed();

    /**
     * Удаляет выгрузку для Facebook
     *
     * @return void
     */
    public function unlinkFeed();
}
