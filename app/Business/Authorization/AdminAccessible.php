<?php

namespace App\Business\Authorization;

/**
 * Админский функционал
 */
interface AdminAccessible
{
    /**
     * Возвращает true, если пользователь является админом
     *
     * @return bool
     */
    public function isAdmin();
}
