<?php

namespace App\Jobs;

use Exception;
use ReflectionClass;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

abstract class Job implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Название джоба
     *
     * @var string
     */
    protected $class;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->startup();
        $this->log('==> START');

        $this->perform();
        $this->log('==> FINISH');
    }

    /**
     * Инициализации перед выполнением джоба
     *
     * @return void
     */
    protected function startup()
    {
        $reflection = new ReflectionClass(get_class($this));
        $this->class = $reflection->getShortName();
    }

    /**
     * Выполняет джоб, до того, как происходит логика завершения выполнения
     *
     * @return void
     */
    abstract protected function perform();

    /**
     * The job failed to process.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->startup();
        $this->preFail($exception);

        $this->log('==> ERROR');
        $this->log('Class: ' . get_class($exception));
        $this->log('Message: ' . $exception->getMessage());
        $this->log('Stack:' . PHP_EOL . $exception->getTraceAsString());

        $this->log('==> FINISH');
    }

    /**
     * Действия перед основной обработкой ошибок
     *
     * @param  \Exception  $ex
     * @return void
     */
    abstract protected function preFail(Exception $ex);

    /**
     * Дописывает запись в лог
     *
     * @param  string  $line
     * @return void
     */
    protected function log(string $line)
    {
        Log::info('Job~' . $this->class . ': ' . $line);
    }
}
