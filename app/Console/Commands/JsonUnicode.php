<?php

namespace App\Console\Commands;

use App\Models\Ad;

class JsonUnicode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'json:unicode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes JSON-encoded strings which content unicode chars in DB';

    /**
     * Тоже самое, что и handle, только ничего не возвращает и IDE-шка не делает мозги, подсвечивая закрывающую скобку
     *
     * @return void
     */
    protected function perform()
    {
        $n = 0;

        foreach (Ad::all() as $ad) {
            /** @var  \App\Models\Ad  $ad */

            $data = $ad->data;
            $validation = $ad->validation;

            $ad->fill([
                'data' => $data ? json_encode($data, JSON_UNESCAPED_UNICODE) : '',
                'validation' => $validation ? json_encode($validation, JSON_UNESCAPED_UNICODE) : null,
            ])->save();

            $n++;
        }

        $this->info('Fixed ads: ' . $n);
    }
}
