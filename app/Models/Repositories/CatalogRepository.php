<?php

namespace App\Models\Repositories;

use App\Http\External\HasFacebookBusinessApi;
use Illuminate\Http\Request;

/**
 * Репозиторий каталогов объявлений
 */
class CatalogRepository extends Repository
{
    use HasFacebookBusinessApi;

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $key = $this->facebookBusinessApi()->createProductCatalog($request->input('name'));
        $feed_id = $this->facebookBusinessApi()->createProductFeed($key);

        $request->merge([
            'key' => $key,
            'feed_id' => $feed_id,
        ]);

        /** @var  \App\Models\Catalog  $catalog */
        $catalog = parent::create($request);

        $catalog->parseFeed($request->file('file'));
        $catalog->exportFeed();

        $this->facebookBusinessApi()->uploadFeed($catalog->feed_id, $catalog->getFeedUrl());

        return $catalog;
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($key, Request $request)
    {
        if ($request->hasFile('file')) {
            $this->destroy($key);
            return $this->create($request);
        }

        $request->merge(['key' => $key]);

        /** @var  \App\Models\Catalog  $catalog */
        $catalog = $this->one($key);

        $this->facebookBusinessApi()->renameProductCatalog($catalog->key, $request->input('name'));

        $catalog->fill($request->all())->save();
        $catalog->exportFeed();

        $this->facebookBusinessApi()->uploadFeed($catalog->feed_id, $catalog->getFeedUrl());

        return $catalog;
    }

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     */
    public function destroy($key)
    {
        /** @var  \App\Models\Catalog  $catalog */
        $catalog = $this->one($key);

        $this->facebookBusinessApi()->deleteProductCatalog($catalog->key);
        $catalog->unlinkFeed();

        return $catalog->delete();
    }
}
