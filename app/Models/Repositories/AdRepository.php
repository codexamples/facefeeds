<?php

namespace App\Models\Repositories;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Репозиторий объявлений
 */
class AdRepository extends Repository
{
    /**
     * Правила валидации для данных объявления
     *
     * @var array
     */
    protected $dataRules = [
        'name' => 'required',
        'image:url' => 'required',
        'address:addr1' => 'required',
        'address:city' => 'required',
        'address:region' => 'required',
        'address:country' => 'required',
        'neighborhood' => 'required',
        'latitude' => 'required',
        'longitude' => 'required',
        'price' => 'required',
        'availability' => 'required',
        'url' => 'required',
    ];

    /**
     * Возвращает все записи модели, или с пагинацией
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Exception
     * @deprecated
     */
    final public function all(Request $request = null)
    {
        throw new Exception('Unable to get all');
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     * @deprecated
     */
    final public function create(Request $request)
    {
        throw new Exception('Unable to create');
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     * @deprecated
     */
    final public function update($key, Request $request)
    {
        throw new Exception('Unable to update');
    }

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     * @throws \Exception
     * @deprecated
     */
    final public function destroy($key)
    {
        throw new Exception('Unable to destroy');
    }

    /**
     * Замещает данные запроса так, как они должны попасть в модель
     *
     * @param  string  $catalog_key
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function mergeRequest(string $catalog_key, Request $request)
    {
        $data = json_encode($request->input('data'), JSON_UNESCAPED_UNICODE);
        $json = json_decode($data, true);

        /** @var  \Illuminate\Validation\Validator  $validator */
        $validator = Validator::make(is_array($json) ? $json : [], $this->dataRules);

        $request->merge([
            'data' => $data,
            'validation' => $validator->fails()
                ? json_encode($validator->messages()->all(), JSON_UNESCAPED_UNICODE)
                : null,
            'catalog_key' => $catalog_key,
        ]);
    }

    /**
     * Возвращает все объявления, привязанные к каталогу
     *
     * @param  string  $catalog_key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function allFromParent(string $catalog_key, Request $request = null)
    {
        $q = $this->startQuery()->where('catalog_key', $catalog_key);

        if ($request && $request->has('q')) {
            $q->where('data', 'like', '%' . $request->input('q') . '%');
        }

        if ($this->perPage) {
            $res = $q->paginate($this->perPage);

            if ($request && $request->has('q')) {
                $res->appends(['q' => $request->input('q')]);
            }

            return $res;
        }

        return $q->get();
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  string  $catalog_key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createFromParent(string $catalog_key, Request $request)
    {
        $this->mergeRequest($catalog_key, $request);
        return parent::create($request);
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  string  $catalog_key
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateFromParent(string $catalog_key, int $id, Request $request)
    {
        $keys = $request->input('keys');
        $values = $request->input('values');
        $data = [];

        if (is_array($keys) && is_array($values) && count($keys) === count($values)) {
            for ($i = 0, $c = count($keys); $i < $c; $i++) {
                if ($keys[$i] && $values[$i]) {
                    $data[$keys[$i]] = $values[$i];
                }
            }
        }

        $request->merge(['data' => $data]);
        $this->mergeRequest($catalog_key, $request);

        return parent::update($id, $request);
    }
}
