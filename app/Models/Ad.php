<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'validation',
        'catalog_key',
    ];

    /**
     * Catalog - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function catalog()
    {
        return $this->belongsTo(Catalog::class, 'catalog_key');
    }

    /**
     * Возвращает данные объявления
     *
     * @return array|null
     */
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data'], true);
    }

    /**
     * Возвращает данные валидации
     *
     * @return array|null
     */
    public function getValidationAttribute()
    {
        return json_decode($this->attributes['validation'], true);
    }
}
