<?php

namespace App\Models;

use App\Business\Data\Feeds\Input as FeedInput;
use App\Business\Data\Feeds\Output as FeedOutput;
use App\Business\Data\Feeds\Valid as FeedValid;
use App\Models\Repositories\AdRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class Catalog extends Model implements FeedInput, FeedOutput, FeedValid
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'name',
        'feed_id',
    ];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Ad - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany(Ad::class, 'catalog_key');
    }

    /**
     * Возвращает репозиторий объявлений
     *
     * @return \App\Models\Repositories\AdRepository
     */
    protected function adRepository()
    {
        return app()->make(AdRepository::class);
    }

    /**
     * Возвращает дескриптор хранилища для выгрузок
     *
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    protected function feedStorage()
    {
        return Storage::drive('feed');
    }

    /**
     * Возвращает название для файла выгрузки
     *
     * @return string
     */
    protected function getFeedName()
    {
        return $this->key . '.xml';
    }

    /**
     * Парсит выгрузку, создавая объекты объявлений
     * Возвращает массив созданных объявлений
     *
     * @param  \Illuminate\Http\UploadedFile  $file
     * @return array
     */
    public function parseFeed(UploadedFile $file)
    {
        $handle = $file->openFile();

        $keys = null;
        $key_count = null;

        $ads = [];
        $ad_repo = $this->adRepository();

        while ($csv = $handle->fgetcsv(';')) {
            if ($keys === null) {
                $keys = array_map(
                    function ($item) {
                        return preg_replace('/[^a-zA-Z0-9-_:]*/', '', $item);
                    },
                    $csv
                );
                $key_count = count($keys);
            } elseif (count($csv) === $key_count) {
                $data = [];

                for ($i = 0; $i < $key_count; $i++) {
                    if (empty($csv[$i])) {
                        continue;
                    }

                    $data[$keys[$i]] = $csv[$i];
                }

                if ($data) {
                    $request = new Request();
                    $request->merge(['data' => $data]);

                    $ads[] = $ad_repo->createFromParent($this->key, $request);
                }
            }
        }

        return $ads;
    }

    /**
     * Возвращает ссылку на выгрузку для Facebook
     *
     * @return string
     */
    public function getFeedUrl()
    {
        return $this->feedStorage()->url($this->getFeedName());
    }

    /**
     * Экспортирует выгрузку для Facebook
     *
     * @return void
     */
    public function exportFeed()
    {
        $listings = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><listings></listings>');
        $ad_repo = $this->adRepository();

        $listings->addChild('title', $this->name);
        $link = $listings->addChild('link');

        $link->addAttribute('rel', 'self');
        $link->addAttribute('href', $this->getFeedUrl());

        foreach ($ad_repo->allFromParent($this->key) as $ad) {
            /** @var  \App\Models\Ad  $ad */

            $listing = $listings->addChild('listing');
            $listing->addChild('home_listing_id', $ad->id);

            $data = $ad->data;

            if (!$data) {
                continue;
            }

            $address = null;
            $image = null;

            foreach ($data as $key => $value) {
                $parts = explode(':', $key);
                $part_count = count($parts);

                if ($parts[0] === 'address' && $part_count > 1) {
                    if ($address === null) {
                        $address = $listing->addChild('address');
                        $address->addAttribute('format', 'simple');
                    }

                    $component = $address->addChild('component', htmlspecialchars($value));
                    $component->addAttribute('name', $parts[1]);
                } elseif ($parts[0] === 'image' && $part_count > 1) {
                    if ($image === null) {
                        $image = $listing->addChild('image');
                    }

                    $image->addChild($parts[1], htmlspecialchars($value));
                } else {
                    $listing->addChild($key, htmlspecialchars($value));
                }
            }
        }

        $this->feedStorage()->put($this->getFeedName(), $listings->asXML());
    }

    /**
     * Удаляет выгрузку для Facebook
     *
     * @return void
     */
    public function unlinkFeed()
    {
        $this->feedStorage()->delete($this->getFeedName());
    }

    /**
     * Возвращает количество невалидных объявлений из выгрузки
     *
     * @return int
     */
    public function getInvalidCount()
    {
        return $this->ads()->whereNotNull('validation')->count();
    }
}
