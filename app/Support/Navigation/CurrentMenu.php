<?php

namespace App\Support\Navigation;

use App\Business\Support\Navigation\CurrentMenu as CurrentMenuContract;
use InvalidArgumentException;

/**
 * Информация о текущих пунктах меню
 *
 * @method string getLeft()
 * @method bool inLeft(string $part, int $level)
 * @method \App\Support\Navigation\CurrentMenu setLeft(string $full)
 * @method \App\Support\Navigation\CurrentMenu addLeft(string $part)
 */
final class CurrentMenu implements CurrentMenuContract
{
    /**
     * Информация обо всех меню
     *
     * @var array
     */
    private $menus;

    /**
     * Создает экземпляр, задавая информацию по умолчанию
     */
    private function __construct()
    {
        $this->menus = [
            'left' => [],
        ];
    }

    /**
     * Вызывает один из возможных методов для управления информацией о меню
     *
     * @param  string  $method
     * @param  array  $args
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __call(string $method, array $args)
    {
        $parts = explode('_', snake_case($method));
        $method = array_shift($parts);

        if (method_exists($this, $method)) {
            array_unshift($args, array_shift($parts));
            return call_user_func_array([$this, $method], $args);
        }

        throw new InvalidArgumentException('Unknown method ' . $method);
    }

    /**
     * Возвращает полную информацию о меню
     *
     * @param  string  $menu
     * @return string
     */
    private function get(string $menu)
    {
        return implode('.', $this->menus[$menu]);
    }

    /**
     * Возвращает true, если часть меню совпадает с указанной
     *
     * @param  string  $menu
     * @param  string  $part
     * @param  int  $level
     * @return bool
     */
    private function in(string $menu, string $part, int $level)
    {
        return count($this->menus[$menu]) > $level && $this->menus[$menu][$level] === $part;
    }

    /**
     * Задает полную информацию о меню
     *
     * @param  string  $menu
     * @param  string  $full
     * @return static
     */
    private function set(string $menu, string $full)
    {
        $this->menus[$menu] = explode('.', $full);
        return $this;
    }

    /**
     * Добавляет в конец часть информации о меню
     *
     * @param  string  $menu
     * @param  string  $part
     * @return static
     */
    private function add(string $menu, string $part)
    {
        $this->menus[$menu][] = $part;
        return $this;
    }

    /**
     * Стартует новый набор информации о меню
     *
     * @return static
     */
    public static function start()
    {
        return new static();
    }
}
