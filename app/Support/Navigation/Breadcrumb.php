<?php

namespace App\Support\Navigation;

use App\Business\Support\Navigation\Breadcrumb as BreadcrumbContract;

/**
 * Хлебная крошка
 */
final class Breadcrumb implements BreadcrumbContract
{
    /**
     * Ссылка хлебной крошки
     *
     * @var string
     */
    private $url;

    /**
     * Заголовок хлебной крошки
     *
     * @var string
     */
    private $caption;

    /**
     * Экземпляр самой первой хлебной крошки
     *
     * @var static
     */
    private $first;

    /**
     * Экземпляр следующей хлебной крошки, за текущей
     *
     * @var static|null
     */
    private $next;

    /**
     * Создает экземпляр, прокидывая в него первичные данные о хлебной крошке
     *
     * @param  string  $url
     * @param  string  $caption
     * @param  \App\Support\Navigation\Breadcrumb|null  $previous
     */
    private function __construct(string $url, string $caption, Breadcrumb $previous = null)
    {
        $this->url = $url;
        $this->caption = $caption;

        if ($previous !== null) {
            $previous->next = $this;
        }
    }

    /**
     * Возвращает ссылку хлебной крошки
     *
     * @return string
     */
    public function url()
    {
        return $this->url;
    }

    /**
     * Возвращает заголовк хлебной крошки
     *
     * @return string
     */
    public function caption()
    {
        return $this->caption;
    }

    /**
     * Возвращает экземпляр самой первой хлебной крошки
     *
     * @return static
     */
    public function first()
    {
        return $this->first;
    }

    /**
     * Возвращает true, если данный экземпляр - самый первый
     *
     * @return bool
     */
    public function isFirst()
    {
        return $this->first() === $this;
    }

    /**
     * Возвращает экземпляр следующей хлебной крошки после текущей
     *
     * @return static|null
     */
    public function next()
    {
        return $this->next;
    }

    /**
     * Возвращает true, если у текущей хлебной крошки есть следующая
     *
     * @return bool
     */
    public function hasNext()
    {
        return $this->next() !== null;
    }

    /**
     * Ищет и возвращает самую последнюю хлебную крошку
     *
     * @return static
     */
    public function last()
    {
        $obj = $this;

        while ($obj->hasNext()) {
            $obj = $obj->next();
        }

        return $obj;
    }

    /**
     * Добавляет новую хлебную крошку в конец
     *
     * @param  string  $url
     * @param  string  $caption
     * @return static
     */
    public function append(string $url, string $caption)
    {
        $obj = new static($url, $caption, $this->last());
        $obj->first = $this->first;

        return $obj;
    }

    /**
     * Создает самую первую хлебную крошку
     *
     * @param  string  $url
     * @param  string  $caption
     * @return static
     */
    public static function start(string $url, string $caption)
    {
        $obj = new static($url, $caption);
        $obj->first = $obj;

        return $obj;
    }
}
