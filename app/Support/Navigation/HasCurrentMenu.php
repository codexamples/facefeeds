<?php

namespace App\Support\Navigation;

use App\Business\Support\Navigation\CurrentMenu;

/**
 * Класс, имеющий информацию о меню, как синглтон
 */
trait HasCurrentMenu
{
    /**
     * Информация о меню
     *
     * @var \App\Business\Support\Navigation\CurrentMenu
     */
    private $currentMenu;

    /**
     * Возвращает информацию о меню
     *
     * @return \App\Business\Support\Navigation\CurrentMenu
     */
    protected function currentMenu()
    {
        if ($this->currentMenu === null) {
            $this->currentMenu = app()->make(CurrentMenu::class);
        }

        return $this->currentMenu;
    }
}
