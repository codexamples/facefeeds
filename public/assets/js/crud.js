$(document).ready(function () {
  /*
  ** INDEX ROUTE DESTROY BUTTON IN POST
  */
  $('.destroy-btn').on('click', function (e) {
    e.preventDefault();

    if (!confirm('Вы уверены в этом?')) {
      return;
    }

    var csrf = $('<input>')
      .prop('type', 'hidden')
      .prop('name', '_token')
      .prop('value', csrfToken);
    var form = $('<form></form>')
      .prop('method', 'POST')
      .prop('action', $(this).prop('href'))
      .append(csrf);

    $('body').append(form);
    form.submit();
  });

  /*
  ** EXPAND/COLLAPSE LARGE TEXT
  */
  var show = '<span class="btn-link">[ Показать текст ]</span>';
  var hide = '<span class="btn-link">[ Скрыть текст ]</span><br><br>';

  $('.col-exp-text').each(function (index, item) {
    item = $(item);
    item
      .data('text-vis', false)
      .data('text-val', item.html())
      .html(show)
      .css('cursor', 'pointer')
      .on('click', function () {
        var self = $(this);

        self
          .html(self.data('text-vis') ? show : hide + self.data('text-val'))
          .data('text-vis', !self.data('text-vis'));
      });
  });
});
