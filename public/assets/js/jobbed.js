window.JOB_COLUMNS = [
  'title',
  'value'
];

$(document).ready(function () {
  $('.job--data, .job--fail').hide();
  requestJobData();
});

function requestJobData() {
  setTimeout(function () {
    $.ajax({
      url: '/api/jobbed',
      type: 'get',
      success: jobDataSucceeded,
      error: jobDataFailed
    });
  }, 400);
}

function jobDataSucceeded(data) {
  if (typeof(data) !== 'object') {
    return jobDataFailed(null, null, 'Invalid data received');
  }

  $('.job--fail').hide();
  $('.job--data').show();

  if (data.active) {
    data.rows.map(function (row) {
      if (row.title.startsWith('job.')) {
        return;
      }

      var id = 'job-row--' + row.key;
      var tr = $('#' + id);

      if (tr.length < 1) {
        tr = $('<tr></tr>')
          .attr('id', id)
          .addClass('job--data');

        $('#job-table').append(tr);

        window.JOB_COLUMNS.map(function () {
          tr.append('<td></td>');
        });
      }

      tr.find('td').each(function (index, td) {
        $(td).html(row[window.JOB_COLUMNS[index]]);
      });
    });

    return requestJobData();
  }

  alert('Готово!');
  location.href = '/';
}

function jobDataFailed(xhr, err, msg) {
  $('.job--data').hide();
  $('.job--fail').show();
  $('#job-row--error').html(msg);

  requestJobData();
}
