<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'IndexController@index')->name('admin');

Route::group(['prefix' => 'password'], function () {
    Route::get('/', 'PasswordController@form')->name('admin.password');
    Route::post('/save', 'PasswordController@save')->name('admin.password.save');
});

Route::group(['prefix' => 'catalog'], function () {
    Route::get('/', 'CatalogController@index')->name('admin.catalog');
    Route::get('create', 'CatalogController@create')->name('admin.catalog.create');
    Route::get('{key}', 'CatalogController@edit')->name('admin.catalog.edit');
    Route::post('store', 'CatalogController@store')->name('admin.catalog.store');
    Route::post('{key}/update', 'CatalogController@update')->name('admin.catalog.update');
    Route::post('{key}/destroy', 'CatalogController@destroy')->name('admin.catalog.destroy');

    Route::group(['prefix' => '{catalog_key}/ad'], function () {
        Route::get('/', 'AdController@index')->name('admin.ad');
        Route::get('{id}', 'AdController@edit')->name('admin.ad.edit');
        Route::post('{id}/update', 'AdController@update')->name('admin.ad.update');
    });
});
