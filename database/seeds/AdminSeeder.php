<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    use SeedsOneUser;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedOneUser('admin', 'admin@admin.admin', 'admin123');
    }
}
